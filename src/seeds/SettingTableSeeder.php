<?php namespace WorkInProgress\Platform;

class SettingTableSeeder extends \Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
    \DB::table('settings')->delete();

    Setting::create(array(
      'key' => 'site_name',
      'title' => 'Site Name',
      'value' => '',
    ));

    Setting::create(array(
      'key' => 'site_description',
      'title' => 'Site Description',
      'value' => '',
    ));

    Setting::create(array(
      'key' => 'site_email',
      'title' => 'Site Email',
      'value' => '',
    ));

    Setting::create(array(
      'key' => 'contact_name',
      'title' => 'Contact Name',
      'value' => '',
    ));

    Setting::create(array(
      'key' => 'social_facebook',
      'title' => 'Facebook URL',
      'value' => '',
    ));

    Setting::create(array(
      'key' => 'social_twitter',
      'title' => 'Twitter URL',
      'value' => '',
    ));

    Setting::create(array(
      'key' => 'social_instagram',
      'title' => 'Instagram URL',
      'value' => '',
    ));

    Setting::create(array(
      'key' => 'social_linkedin',
      'title' => 'Linkedin URL',
      'value' => '',
    ));
	}

}
