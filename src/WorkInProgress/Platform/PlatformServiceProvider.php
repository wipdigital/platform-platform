<?php namespace WorkInProgress\Platform;

use Illuminate\Support\ServiceProvider;

class PlatformServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('work-in-progress/platform', 'platform');

    include __DIR__ . '/../../routes.php';

    $packages = $this->app->make('packages');
    $this->app->make('session')->put('packages', $packages->read());
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
    $this->app->singleton('packages', function() {
      return new Packages;
    });

    $this->app['packages']->add('Dashboard','/platform');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
    //@TODO Don't think this is necessary?
		return array('platform.packages');
	}

}
