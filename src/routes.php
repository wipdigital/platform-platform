<?php

Route::controller('settings', 'WorkInProgress\Platform\SettingsController');
Route::resource('platform/upload', 'WorkInProgress\Platform\UploadController');
Route::controller('platform', 'WorkInProgress\Platform\PlatformController');

?>
