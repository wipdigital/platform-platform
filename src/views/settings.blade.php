@extends('layouts.standard')

@section('main')
  <h1>Settings</h1>

  @if(Session::has('message'))
  <div data-alert class="alert-box success">
    {{ Session::get('message') }}
    <a href="#" class="close">&times;</a>
  </div>
  @endif

  @if($errors->first())
    <div class="alert-box">
      <h2>Error</h2>

      @foreach($errors->all() as $error)
      <p>{{ $error }}</p>
      @endforeach
    </div>
  @endif

  {{ Form::model($settings, ['url' => 'settings', 'data-abide']) }}
    <h2>Site Details</h2>
    <div class="site-name-field @if($errors->has('site_name'))error @endif">
      <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('platform::settings.site_name') }}">{{ Form::label('site_name', 'Site Name') }}</span>
      {{ Form::text('site_name', null, ['class' => 'radius', 'required']) }}
      @if($errors->has('site_name'))
      <small class="error">{{ $errors->first('site_name') }}</small>
      @endif
      <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Site Name']) }}</small>
    </div>

    <div class="site-description-field @if($errors->has('site_description'))error @endif">
      <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('platform::settings.site_description') }}">{{ Form::label('site_description', 'Site Description') }}</span>
      {{ Form::textarea('site_description', null, ['class' => 'radius']) }}
      @if($errors->has('site_description'))
      <small class="error">{{ $errors->first('site_description') }}</small>
      @endif
      <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Site Description']) }}</small>
    </div>

    <div class="authentication button-group right">
      <a href="/authentication/instagram" class="medium button">
        @if(\Setting::where('key', 'instagram_access_token')->first())
          <span class="fa fa-check"></span>
        @endif
         Instagram Authentication
      </a>

      <a href="/authentication/kounta" class="medium button">
        @if(\Setting::where('key', 'kounta_refresh_token')->first())
          <span class="fa fa-check"></span>
        @endif
         Kounta Authentication
      </a>
    </div>

    <h2>Contact Details</h2>
    <div class="contact-name-field @if($errors->has('contact_name'))error @endif">
      <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('platform::settings.contact_name') }}">{{ Form::label('contact_name', 'Contact Name') }}</span>
      {{ Form::text('contact_name', null, ['class' => 'radius', 'required']) }}
      @if($errors->has('contact_name'))
      <small class="error">{{ $errors->first('contact_name') }}</small>
      @endif
      <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Contact Name']) }}</small>
    </div>

    <div class="site-email-field @if($errors->has('site_email'))error @endif">
      <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('platform::settings.site_email') }}">{{ Form::label('site_email', 'Contact Email') }}</span>
      {{ Form::email('site_email', null, ['class' => 'radius', 'required']) }}
      @if($errors->has('site_email'))
      <small class="error">{{ $errors->first('site_email') }}</small>
      @endif
      <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Site Email']) }}</small>
    </div>

    <h2>Social Details</h2>
    {{ Form::label('social_facebook', 'Facebook URL') }}
    <div class="row collapse prefix-radius">
      <div class="small-3 columns">
        <span class="prefix">https://www.facebook.com/</span>
      </div>
      <div class="small-9 columns radius-fx">
        {{ Form::text('social_facebook') }}
      </div>
    </div>

    {{ Form::label('social_twitter', 'Twitter URL') }}
    <div class="row collapse prefix-radius">
      <div class="small-3 columns">
        <span class="prefix">https://www.twitter.com/</span>
      </div>
      <div class="small-9 columns radius-fx">
        {{ Form::text('social_twitter') }}
      </div>
    </div>

    {{ Form::label('social_instagram', 'Instagram URL') }}
    <div class="row collapse prefix-radius">
      <div class="small-3 columns">
        <span class="prefix">https://www.instagram.com/</span>
      </div>
      <div class="small-9 columns radius-fx">
        {{ Form::text('social_instagram') }}
      </div>
    </div>

    {{ Form::label('social_linkedin', 'LinkedIn URL') }}
    <div class="row collapse prefix-radius">
      <div class="small-3 columns">
        <span class="prefix">https://www.linkedin.com/</span>
      </div>
      <div class="small-9 columns radius-fx">
        {{ Form::text('social_linkedin') }}
      </div>
    </div>

    {{ Form::submit('Save', array('class' => 'button right small')) }}
  {{ Form::close() }}
@stop
