@extends('layouts.standard')

@section('main')
<div class="dash-bg"></div>
<div class="row collapse">
	<div class="small-12 medium-7 columns">
		<img src="/images/dash-logo.png" class="dash-logo">
	</div>
</div>

<div class="row dash">
	<div class="small-12 large-6 columns">
		<div class="row">
			<div class="small-2 columns">
				<span class="increase"><i class="fa fa-circle fa-stack-2x fa-inverse op"></i><i class="fa fa-tachometer fa-inverse fa-stack-1x fx"></i></span>
			</div>
			<div class="small-10 columns">
				<h3>DASHBOARD</h3>
				<p>All the latest stats and notifications from your site. Customise it to view what is most important to you.</p>
			</div>
		</div>
	</div>
	<div class="small-12 large-6 columns">
		<div class="row">
			<div class="small-2 columns">
				<span class="increase"><i class="fa fa-circle fa-stack-2x fa-inverse op"></i><i class="fa fa-file-text fa-inverse fa-stack-1x fx"></i></span>
			</div>
			<div class="small-10 columns">
				<h3>PAGES</h3>
				<p>Create, edit and delete your content from anywhere. Pages is at the very core of Platform&trade;.</p>
			</div>
		</div>
	</div>
	<div class="small-12 large-6 columns">
		<div class="row">
			<div class="small-2 columns">
				<span class="increase"><i class="fa fa-circle fa-stack-2x fa-inverse op"></i><i class="fa fa-shopping-cart fa-inverse fa-stack-1x fx"></i></span>
			</div>
			<div class="small-10 columns">
				<h3>PRODUCTS</h3>
				<p>At the heart of Platforms&trade; E-Commerce software. with advanced features.</p>
			</div>
		</div>
	</div>
	<div class="small-12 large-6 columns">
		<div class="row">
			<div class="small-2 columns">
				<span class="increase"><i class="fa fa-circle fa-stack-2x fa-inverse op"></i><i class="fa fa-truck fa-inverse fa-stack-1x fx"></i></span>
			</div>
			<div class="small-10 columns">
				<h3>SALES</h3>
				<p>At the heart of Platforms&trade; E-Commerce software. Take full control over your online business. </p>
			</div>
		</div>
	</div>
	<div class="small-12 large-6 columns left">
		<div class="row">
			<div class="small-2 columns">
				<span class="increase"><i class="fa fa-circle fa-stack-2x fa-inverse op"></i><i class="fa fa-group fa-inverse fa-stack-1x fx"></i></span>
			</div>
			<div class="small-10 columns">
				<h3>CUSTOMERS</h3>
				<p>All your E-Commerce customers at a glance.</p>
			</div>
		</div>
	</div>
		<div class="small-12 large-6 columns">
		<div class="row">
			<div class="small-12 columns">
					<a href="/" class="tour button">TAKE THE TOUR <i class="fa fa-paper-plane"></i></a>
			</div>
		</div>
	</div>				
</div><!--End row-->

<ol class="joyride-list" data-joyride>
  <li data-text="Next" data-options="prev_button: false">
    <h2>Hello,</h2>
    <p>Welcome to the Platform&trade; tour.</p>
  </li>
  <li data-class="Dashboard" data-text="Next" data-prev-text="Prev" data-options="tip_animation:fade">
    <h4>Main Menu</h4>
    <p>This is where you will find all the different types of editable content that are avialable to you.</p>
  </li>
  <li data-class="has-dropdown" data-text="Next" data-prev-text="Prev" data-options="tip_location:right; tip_animation:fade">
    <h4>Account Menu</h4>
    <p>Here you can log out or adjust the Platform&trade; settings.</p>
  </li>
  <li data-button="End" data-prev-text="Prev">
    <h4>Thanks</h4>
    <p>Your tour is now over. If you need any further information visit <a href="http://platform.wip.technology/documentation" target="_blank">our documentation</a>.</p>
  </li>
</ol>
@stop

@section('inline_js')
<script>
  $(document).on('click', '.tour', function(e) {
    $(document).foundation('joyride', 'start');
    e.preventDefault();
  });
</script>
@stop
