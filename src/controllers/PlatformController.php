<?php namespace WorkInProgress\Platform;

class PlatformController extends \BaseController {

  public function __construct()
  {
    $this->beforeFilter('auth');
  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
  {
    return \View::make('platform::index');
	}

}
