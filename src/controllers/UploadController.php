<?php namespace WorkInProgress\Platform;

class UploadController extends \BaseController {

  public function __construct()
  {
    $this->beforeFilter('auth');
  }

  public function index()
  {
    // Array of image links to return.
    $response = array();

    // Image types.
    $image_types = array(
      "image/gif",
      "image/jpeg",
      "image/pjpeg",
      "image/jpeg",
      "image/pjpeg",
      "image/png",
      "image/x-png"
    );

    // Filenames in the uploads folder.
    $destinationPath = public_path() . '/upload/' . \Auth::id() .'/';
    $fnames = scandir($destinationPath);

    // Check if folder exists.
    if ($fnames) {
      // Go through all the filenames in the folder.
      foreach ($fnames as $name) {
        // Filename must not be a folder.
        if (!is_dir($name)) {
          // Check if file is an image.
          if (in_array(mime_content_type($destinationPath . $name), $image_types)) {
            // Add to the array of links.
            array_push($response, "/upload/" . \Auth::id() . '/' . $name);
          }
        }
      }

      return \Response::json($response);
    }

    return \Response::json(array('error' => 'Images folder does not exist!'), 404);
  }

	public function store()
  {
    if(\Input::hasFile('file') && \Input::file('file')->isValid()) {

      $destinationPath = 'upload/' . \Auth::id() . '/';

      if(\Input::file('file')->move($destinationPath, \Input::file('file')->getClientOriginalName())) {
        return \Response::json(array('link' => '/' . $destinationPath . \Input::file('file')->getClientOriginalName()), 200);
      }
    }

    return \Response::json(array('error' => 'No valid file uploaded'), 404);
	}

}
