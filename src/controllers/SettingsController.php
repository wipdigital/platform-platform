<?php namespace WorkInProgress\Platform;

class SettingsController extends \BaseController {

	private $rules = array(
    'site_name' => 'required',
    'site_email' => 'required|email'
  );

  public function __construct()
  {
    $this->beforeFilter('auth');
  }

  public function postIndex()
  {
    $data = \Input::all();

    $validator = \Validator::make($data, $this->rules);

    if($validator->fails()) {
      return \Redirect::to('settings')->withErrors($validator)->withInput();
    }

    \DB::table('settings')->where('key', 'site_name')->update(array('value' => \Input::get('site_name')));
    \DB::table('settings')->where('key', 'site_description')->update(array('value' => \Input::get('site_description')));
    \DB::table('settings')->where('key', 'site_email')->update(array('value' => \Input::get('site_email')));
    \DB::table('settings')->where('key', 'contact_name')->update(array('value' => \Input::get('contact_name')));
    \DB::table('settings')->where('key', 'social_facebook')->update(array('value' => \Input::get('social_facebook')));
    \DB::table('settings')->where('key', 'social_twitter')->update(array('value' => \Input::get('social_twitter')));
    \DB::table('settings')->where('key', 'social_instagram')->update(array('value' => \Input::get('social_instagram')));
    \DB::table('settings')->where('key', 'social_linkedin')->update(array('value' => \Input::get('social_linkedin')));

    \Session::flash('message', 'Settings updated!');
    return \Redirect::to('settings');
  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
  {
    $data = array(
      'settings' => array(
        'site_name' => \DB::table('settings')->where('key', 'site_name')->pluck('value'),
        'site_description' => \DB::table('settings')->where('key', 'site_description')->pluck('value'),
        'site_email' => \DB::table('settings')->where('key', 'site_email')->pluck('value'),
        'contact_name' => \DB::table('settings')->where('key', 'contact_name')->pluck('value'),
        'social_facebook' => \DB::table('settings')->where('key', 'social_facebook')->pluck('value'),
        'social_twitter' => \DB::table('settings')->where('key', 'social_twitter')->pluck('value'),
        'social_instagram' => \DB::table('settings')->where('key', 'social_instagram')->pluck('value'),
        'social_linkedin' => \DB::table('settings')->where('key', 'social_linkedin')->pluck('value')
      )
    );

    return \View::make('platform::settings', $data);
	}

}
