<?php namespace WorkInProgress\Platform;

class Packages {

  private $packages = array();

  public function add($name, $url) {
    $this->packages[] = array('name' => $name, 'url' => $url);
  }

  public function read() {
    return $this->packages;
  }
}
