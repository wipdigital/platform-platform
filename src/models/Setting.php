<?php namespace WorkInProgress\Platform;

class Setting extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'settings';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = ['id'];

}

?>
